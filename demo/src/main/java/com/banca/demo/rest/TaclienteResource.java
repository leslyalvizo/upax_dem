package com.banca.demo.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.banca.demo.model.Tacliente;
import com.banca.demo.service.TaclienteService;
import javax.validation.Valid;
import java.net.URISyntaxException;

public class TaclienteResource {

    private static final Logger log = LoggerFactory.getLogger(TaclienteResource.class);

    @Autowired
    private TaclienteService service;
    
    @GetMapping("/tacliente")
    public ResponseEntity<List<Tacliente>> getAllTacliente(@PathVariable Pageable pageable) {
        log.debug("REST request to get a page of Tacliente");
        Page<Tacliente> page = service.findAll(pageable);
        return new ResponseEntity<>(page.getContent(), HttpStatus.OK);
    }

    @GetMapping("/tacliente/{id}")
    public ResponseEntity<Tacliente> getTacliente(@PathVariable Integer id) {
        log.debug("REST request to get Tacliente : {}", id);
        Tacliente tacliente = service.findById(id);
        return new ResponseEntity<Tacliente>(tacliente, HttpStatus.OK);
    }

    @PostMapping("/tacliente")
    public ResponseEntity<Tacliente> createTacliente(@Valid @RequestBody Tacliente tacliente) throws URISyntaxException {
        log.debug("REST request to save Tacliente : {}", tacliente);
        Tacliente result = service.save(tacliente);
        return new ResponseEntity<Tacliente>(result, HttpStatus.OK);
    }

    @PutMapping("/tacliente")
    public ResponseEntity<Tacliente> updateTacliente(@Valid @RequestBody Tacliente tacliente) throws URISyntaxException {
        log.debug("REST request to update Tacliente : {}", tacliente);
        Tacliente result = service.save(tacliente);
        return new ResponseEntity<Tacliente>(result, HttpStatus.OK);
    }

    @DeleteMapping("/tacliente/{id}")
    public ResponseEntity<Void> deleteTacliente(@PathVariable Integer id) {
        log.debug("REST request to delete Tacliente : {}", id);
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
