package com.banca.demo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "tacliente")
public class Tacliente implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "finumero", unique = true, nullable = false)
    private Integer finumero;
    
    @Column(name = "fcnombre")
    private String fcnombre;
    
    @Column(name = "fcfechanacimiento")
    private Date fcfechanacimiento;
    
    @Column(name = "fcrfc")
    private String fcrfc;
    
    @ManyToOne
    @Column(name = "fidomicilio")
    private Tadomicilio fidomicilio;
    
    @Column(name = "fiestadocliente")
    private Integer fiestadocliente;
    
    @ManyToOne
    @Column(name = "fibanco")
    private Tabanco fibanco;

	public Integer getFinumero() {
		return finumero;
	}

	public void setFinumero(Integer finumero) {
		this.finumero = finumero;
	}

	public String getFcnombre() {
		return fcnombre;
	}

	public void setFcnombre(String fcnombre) {
		this.fcnombre = fcnombre;
	}

	public Date getFcfechanacimiento() {
		return fcfechanacimiento;
	}

	public void setFcfechanacimiento(Date fcfechanacimiento) {
		this.fcfechanacimiento = fcfechanacimiento;
	}

	public String getFcrfc() {
		return fcrfc;
	}

	public void setFcrfc(String fcrfc) {
		this.fcrfc = fcrfc;
	}

	public Tadomicilio getFidomicilio() {
		return fidomicilio;
	}

	public void setFidomicilio(Tadomicilio fidomicilio) {
		this.fidomicilio = fidomicilio;
	}

	public Integer getFiestadocliente() {
		return fiestadocliente;
	}

	public void setFiestadocliente(Integer fiestadocliente) {
		this.fiestadocliente = fiestadocliente;
	}

	public Tabanco getFibanco() {
		return fibanco;
	}

	public void setFibanco(Tabanco fibanco) {
		this.fibanco = fibanco;
	}
    
    

}
