package com.banca.demo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tadomicilio")
public class Tadomicilio implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "finumero", unique = true, nullable = false)
	private Integer finumero;
    
    @Column(name = "fccalle")
	private String fccalle;
    
    @Column(name = "fccolonia")
	private String fccolonia;
    
    @Column(name = "fcestado")
	private String fcestado;
    
    @Column(name = "ficodigopostal")
	private String ficodigopostal;
    
    @Column(name = "fiestadodomicilio")
	private String fiestadodomicilio;

	public Integer getFinumero() {
		return finumero;
	}

	public void setFinumero(Integer finumero) {
		this.finumero = finumero;
	}

	public String getFccalle() {
		return fccalle;
	}

	public void setFccalle(String fccalle) {
		this.fccalle = fccalle;
	}

	public String getFccolonia() {
		return fccolonia;
	}

	public void setFccolonia(String fccolonia) {
		this.fccolonia = fccolonia;
	}

	public String getFcestado() {
		return fcestado;
	}

	public void setFcestado(String fcestado) {
		this.fcestado = fcestado;
	}

	public String getFicodigopostal() {
		return ficodigopostal;
	}

	public void setFicodigopostal(String ficodigopostal) {
		this.ficodigopostal = ficodigopostal;
	}

	public String getFiestadodomicilio() {
		return fiestadodomicilio;
	}

	public void setFiestadodomicilio(String fiestadodomicilio) {
		this.fiestadodomicilio = fiestadodomicilio;
	}
    
    
}
