package com.banca.demo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tabanco")
public class Tabanco implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "finumero", unique = true, nullable = false)
	private Integer finumero;
    
    @Column(name = "fcnombre")
	private String fcnombre;
    
    @Column(name = "fcrfc")
	private String fcrfc;
    
    @Column(name = "fctelefono")
	private String fctelefono;
    
    @Column(name = "fiestadobanco")
	private String fiestadobanco;
    
    @ManyToOne
    @JoinColumn(name = "fidomicilio")
    private Tadomicilio fidomicilio;

	public Integer getFinumero() {
		return finumero;
	}

	public void setFinumero(Integer finumero) {
		this.finumero = finumero;
	}

	public String getFcnombre() {
		return fcnombre;
	}

	public void setFcnombre(String fcnombre) {
		this.fcnombre = fcnombre;
	}

	public String getFcrfc() {
		return fcrfc;
	}

	public void setFcrfc(String fcrfc) {
		this.fcrfc = fcrfc;
	}

	public String getFctelefono() {
		return fctelefono;
	}

	public void setFctelefono(String fctelefono) {
		this.fctelefono = fctelefono;
	}

	public String getFiestadobanco() {
		return fiestadobanco;
	}

	public void setFiestadobanco(String fiestadobanco) {
		this.fiestadobanco = fiestadobanco;
	}

	public Tadomicilio getFidomicilio() {
		return fidomicilio;
	}

	public void setFidomicilio(Tadomicilio fidomicilio) {
		this.fidomicilio = fidomicilio;
	}

   
    

}
