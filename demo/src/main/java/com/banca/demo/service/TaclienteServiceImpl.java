package com.banca.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.transaction.annotation.Transactional;

import com.banca.demo.model.Tacliente;
import com.banca.demo.repository.TaclienteRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaclienteServiceImpl implements TaclienteService{


    private final Logger log = LoggerFactory.getLogger(TaclienteServiceImpl.class);

    @Autowired
    private TaclienteRepository repository;

    @Override
    @Transactional(readOnly = true)
    public Page<Tacliente> findAll(Pageable pageable) {
        log.debug("Request to get all Tacliente");
        return repository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Tacliente findById(Integer id) {
        log.debug("Request to get Tacliente : {}", id);
        return repository.findById(id);
    }

    @Override
    public Tacliente save(Tacliente tacliente) {
        return repository.save(tacliente);
    }

    @Override
    public void delete(Integer id) {
        log.debug("Request to delete Tacliente : {}", id);
        repository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        log.debug("Request to delete All Tacliente");
        repository.deleteAll();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Page<Tacliente> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Tacliente");
        return repository.findAll(pageable);
    }

	@Override
	public boolean exists(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

}
