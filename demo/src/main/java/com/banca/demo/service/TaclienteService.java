package com.banca.demo.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.banca.demo.model.Tacliente;

public interface TaclienteService {

    Page<Tacliente> findAll(Pageable pageable);

    Tacliente findById(Integer id);

    Tacliente save(Tacliente usuarioUc);

    boolean exists(Integer id);

    void delete(Integer id);

    void deleteAll();
    
    Page<Tacliente> search(String query, Pageable pageable);
    
}
