package com.banca.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.banca.demo.model.Tacliente;

public interface TaclienteRepository extends JpaRepository<Tacliente, Long> {

	Tacliente findById(Integer id);

	void deleteById(Integer id);

}
